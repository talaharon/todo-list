import JsonDB from "./json-database.js";
import * as utils from './utils.js';

const [COMPLETED, OPEN] = ['completed', 'open'];

export default class ToDoList {
    private static _DB : JsonDB = new JsonDB('./todos.json');

    private constructor() {
        // NONE - Make instance initiation unavailable
    }

    /**********************
     * Public functions
     *********************/

    static read = async (filter: string): Promise<void> => {
        let tasks: ITask[] = await this._DB.read();
        let filterFunc: (task: ITask) => boolean;
        switch (filter.toLowerCase()) {
            case COMPLETED:
                filterFunc = (task: ITask) => !task.isActive;
                break;
            case OPEN:
                filterFunc = (task: ITask) => !!task.isActive;
                break;
            default:
                filterFunc = (task: ITask) => true;
                break;
        }
        this.printTasks(tasks.filter(filterFunc));
    };

    static create = async (title: string): Promise<void> => {
        let tasks: ITask[] = await this._DB.read();
        tasks.push({
            title,
            id: utils.uidGenerator(),
            isActive: true
        });
        await this._DB.write(tasks);
    }

    static update = async (id: string): Promise<void> => {
        let tasks: ITask[] = await this._DB.read();
        tasks.forEach((task: ITask): void => {
            if (task.id === id) {
                task.isActive = !task.isActive;
            }
        });
        await this._DB.write(tasks);
    }

    static remove = async (id: string): Promise<void> => {
        let tasks: ITask[] = await this._DB.read();
        tasks = tasks.filter((task: ITask): boolean => {
            return task.id !== id;
        });
        await this._DB.write(tasks);
    }

    static removeAllCompleted = async (): Promise<void> => {
        let tasks: ITask[] = await this._DB.read();
        await this._DB.write(tasks.filter((task: ITask) => {
            return task.isActive;
        }));
    }

    /**********************
     * Private functions
     *********************/

    private static printTasks = (tasks: ITask[]): void => {
        console.table(tasks.map((task: ITask): ITask => {
            task.isActive = task.isActive ? "\u2610" : "\u2705";
            return task;
        }));
    }
}
