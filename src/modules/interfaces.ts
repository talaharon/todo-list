interface ICommand {
    title: string;
    arguments: IArguments;
}

interface IArguments {
    [key: string]: string;
}

interface IHelpInfo {
    description: string,
    parameters: string,
    examples: string
}
interface IHelpObj {
    [key: string]: IHelpInfo;
}

interface ITask {
    id: string;
    title: string;
    isActive: string | boolean;
}
