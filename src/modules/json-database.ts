import fs from 'fs/promises';
import log from '@ajar/marker';

export default class JsonDB {
    private _DBpath: string = './default.json';

    constructor(DBpath: string) {
        this._DBpath = DBpath;
    }

    public write = async (data: any): Promise<boolean> => {
        try {
            await fs.writeFile(this._DBpath, JSON.stringify(data), "utf-8");
            return true;
        } catch {
            // log.err(`Failed to write to '${this._DBpath}'`);
            return false;
        }
    };

    public read = async (): Promise<any> => {
        try {
            const data = await fs.readFile(this._DBpath, "utf-8");
            return JSON.parse(data);
        } catch (err) {
            log.err(`Failed to read from '${this._DBpath}'`);
            return [];
        }
    };

    public erase = async (): Promise<boolean> => {
        try {
            await fs.unlink(this._DBpath);
            return true;
        } catch (error) {
            log.err(`Failed to delete from '${this._DBpath}'`);
            return false;
        }
    }


}