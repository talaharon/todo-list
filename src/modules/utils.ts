import prompt from 'prompt-sync';
import log from '@ajar/marker';

// Generates a random string id
export const uidGenerator = (): string => Math.random().toString(16).substring(2);


// Get input from console
export const askInput = (inputRequired: string): string => {
    let waitForInput = true;
    let input = '';
    while (waitForInput) {
        input = prompt()(`Please enter a valid ${inputRequired}: `);
        if (input != '') {
            waitForInput = false;
            break;
        }
        log.red(`${inputRequired} is empty, please try again...`)
    }
    return input;
}